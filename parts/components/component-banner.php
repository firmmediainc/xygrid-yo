<section id="page-header">
<?php 
// set the acf fields to avariable to make them easier to work with below
$bannerImg = get_field('banner_image');
$bannerContent = get_field('banner_content');
$bannerHeight = get_field('banner_min_height');
?>

<?php if ($bannerImg):?>
	<?php $ctr = 0; foreach($bannerImg  as $image ): $ctr++; ?>							 
		<?php $bannerimg[$ctr] = $image['url']; ?>					
	<?php endforeach; ?>
<?php endif;?>	

<div class="marketing-site-hero" <?php if ($bannerImg):?> data-interchange="[<?=$bannerimg[3];?>, small], [<?=$bannerimg[2];?>, medium], [<?=$bannerimg[1];?>, large]" <?php endif;?>>
	<div class="grid-container grid-container-padded">
	<div class="grid-x grid-margin-x grid-padding-x align-middle marketing-site-hero-content" style="min-height: <?=$bannerHeight;?>;">
		<div class="cell small-12 large-8">
			<?php if ( $bannerContent ):?>
				<div class="bannerText">
					<?=$bannerContent;?>	
				</div>
			<?php endif;?>
		</div>		
	</div>
	</div>

</div>
	
</section>
